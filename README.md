wp-mat
===

Wordpress plugin that automatically filters all uploaded files through
[MAT2](https://0xacab.org/jvoisin/mat2) in order to remove potentially
identifying metadata.

Meant to be used in combination with
[wp-mat-server](https://git.autistici.org/noblogs/wp-mat-server), a
simple API wrapper for mat2.

### Configuration

Define these in wp-config.php to control the plugin behavior:

* `WP_MAT_ENDPOINT` - full URL to the *wp-mat-server* instance to use
  (including the */api/cleanup* path).

